"use strict";
/**
 * Vegasプラグインでヘッダー画像のスライドショーを作成
 */
$(function(){
      $('#header').vegas({
            slides: [
                  {src: 'img/download01.jpeg'},
                  {src: 'img/download02.jpeg'},
                  {src: 'img/download03.jpeg'},
                  {src: 'img/download04.jpeg'},
                  {src: 'img/download05.jpeg'},
                  {src: 'img/download06.jpeg'},
                  {src: 'img/download07.jpeg'},
                  {src: 'img/download08.jpeg'},
            ],
            overlay: './js/overlays/02.png',
            transition: 'fade',
            transitionDuration: 7000,
            delay: 10000,
            animation: 'random',
            animationDuration: 200000,
      });
});

// window.onload = function(){
//   var typing = document.getElementById("typing").innerHTML;
//   document.getElementById("typing").style.display = "none";
//   const typingNone = document.getElementById("typing");
//   if(typingNone.style.display != "block"){
//     for(var i = 0; i<8; i++){
//       typing = typing.charAt(i);
//       alert(typing);
//     }
//   }
// }

function typing(str = ""){
  let buf = document.getElementById("typing").innerHTML; //書き込み済みの文字を要素から取得
  let writed = buf.length; //書き込み済みの文字数を取得
  let write = "";
  if(writed < str.length){
      write = str.charAt(writed); //1文字だけ取得する
  }
  document.getElementById("typing").innerHTML = buf + write; //1文字だけ追加していく
}

const str = document.getElementById("typing").innerHTML; //書き込む文字を要素から取得
const delay = 200 //1文字が表示される時間

document.getElementById("typing").innerHTML = "";
window.setInterval(function(){typing(str);}, delay);